#! /bin/bash
#
# Requires: xmlstarlet
##########################################

ROOT_DIR="`dirname $0`"

COMPOSITE_UPSITE_URL="http://gecos.gforge.inria.fr/updatesite/composite/releases"

##########################################
## PARAMETERS

BUILD_DIR="$ROOT_DIR/build"
TARGET_FILE="$ROOT_DIR/releng/fr.irisa.cairn.gecos.release.oxygen.target/fr.irisa.cairn.gecos.release.oxygen.target.target"
POM_ID="fr.irisa.cairn.gecos.composite"
POM_VERSION="1.0.0-SNAPSHOT"

# if 1 copy projects to BUILD_DIR else create sym links
COPY=0

# if 1 also create a non-composite updatesite using the agregated sources
# otherwise, create a composite updatesite with links to the specified submodules 
# release updatesite URLs.
CREATE_UPSITE_FROMSOURCE=0

SUBMODULES=`ls -d $ROOT_DIR/gecos-*`


processParams() {
    while getopts "hcst:b:i:v:" opt "$@"; do
        case "$opt" in
        h)
            showHelp
            exit 0
            ;;
        t) TARGET_FILE="$OPTARG";;
        b) BUILD_DIR="$OPTARG";;
        i) POM_ID="$OPTARG";;
        v) POM_VERSION="$OPTARG";;
        c) COPY=1;;
        s) CREATE_UPSITE_FROMSOURCE=1;;
        esac
    done

    shift $((OPTIND-1))
    [ "$1" = "--" ] && shift

	[ -z "$1" ] || SUBMODULES="$@"

    printParams

	if [ -z "$SUBMODULES" ]; then
		error "No specified submodules!"
        exit 1
	fi
    if [ ! -f "$TARGET_FILE" ]; then
        error "Specified TARGET_FILE is not found at '$TARGET_FILE'!"
        exit 1
    fi
}

showHelp() {
    echo "This commad can be used to create a composite build directory by gathering"
    echo "all sources (bundles/features/tests) from all the specified GeCoS submodules."
    echo ""
    echo "Usage: $0 [OPTIONS]... [SUBMODULES]"
    echo ""
    echo "SUBMODULES is the list of submodules to include."
    echo "By default all submodules ($ROOT_DIR/gecos-*) are included."
	echo ""
    echo "Options:"
	echo "  -t TARGET       Specify the Target Plateform Definition file to build against."
	echo "                     DEFAULT value: '$ROOT_DIR/releng/fr.irisa.cairn.gecos.release.oxygen.target/fr.irisa.cairn.gecos.release.oxygen.target.target'"
    echo "  -s              When this option is used, a non-composite updatesite using directly the"
	echo "                     agregated sources is created. Otherwise, by DEFAULT, a composite updatesite"
	echo "                     simply linking to the specified submodules updates sites is created (using"
	echo "                     '$ROOT_DIR/releng/$POM_ID.update')."  
	echo "  -i POM_ID       Specify the POM ID prefix to be used."
	echo "                     DEFAULT value: 'fr.irisa.cairn.gecos.composite'."
	echo "  -v POM_VERSION  Specify the POM version to be used."
	echo "                     DEFAULT value: '1.0.0-SNAPSHOT'."
    echo "  -c              When this option is used, sources are COPIED from submodules to the composite"
	echo "                     build directory. Otherwise, by DEFAULT, symbolic links are used."
	echo "  -b BUILD_DIR    Specify the location where to generate the composite build directory."
	echo "                     DEFAULT value: '$ROOT_DIR/build'."
    echo "  -h              Display this help message and exit."
}

printParams() {
    info "PARAMETERS:"
	info "  - SUBMODULES               = '$SUBMODULES'"
    info "  - BUILD_DIR                = '$BUILD_DIR'"
    info "  - TARGET_FILE              = '$TARGET_FILE'"
    info "  - POM_ID                   = '$POM_ID'"
    info "  - POM_VERSION              = '$POM_VERSION'"
    info "  - COPY                     = $COPY"
    info "  - CREATE_UPSITE_FROMSOURCE = $CREATE_UPSITE_FROMSOURCE"
}


##########################################
main() {
	processParams "$@"

	rm -r "$BUILD_DIR"
	create_composite_build_dir

#	local mvnOptions=$@
#	[ -z "$mvnOptions" ] && exit 0
#	cd "$BUILD_DIR" && 	mvn $mvnOptions
}

create_composite_build_dir() {
	# clean build dir
	info "Cleaning build directory..."
	rm -rf "$BUILD_DIR"
	
	# copy bundles, features and tests from all submodules
	init bundles
	init features
	init tests

	# create update site as aggregate of all projects
	aggregate_releng

	# create root pom file
	local pomFile="$BUILD_DIR/pom.xml"
	local GAV="$(getGAV "$POM_ID.root" "pom" "$POM_ID" "$POM_VERSION")"
	local parentGAV="$(getGAV "fr.irisa.cairn.gecos.buildtools.root" "" "fr.irisa.cairn.gecos.buildtools" "1.0.0-SNAPSHOT" "../gecos-buildtools")"
	local modules="bundles features tests releng"
	local properties="
		<target.groupId>$POM_ID</target.groupId>
    	<target.artifactId>$POM_ID.target</target.artifactId>
    	<target.version>$POM_VERSION</target.version>"

	create_pom "$pomFile" "$GAV" "$parentGAV" "$modules" "$properties"
}	

# $1: modules name (bundles, features or tests)
init() {
	local name="$1"
	local dest="$BUILD_DIR/$name"

	info "Creating $dest..."

	mkdir -p "$dest"

	local originalPom
	local moduleName
	local modules
	for sub in $SUBMODULES; do
		[ "`basename $sub`" == "gecos-buildtools" ] && continue
		[ ! -d "$sub/$name/" ] && continue
		originalPom="$sub/$name/pom.xml"
		for dir in `find "$sub/$name/" -maxdepth 1 -mindepth 1 -type d`; do
			if [ "$COPY" == "1" ]; then
				cp -r "$dir" "$dest"
			else
				ln -s "`realpath $dir`" "$dest"
			fi
			moduleName=`basename $dir`
			## only add modules that appears in the original pom.xml
			if [ -f "$originalPom" ] && [ ! -z "`grep "<module>\s*${moduleName}\s*<\/module>"  "$originalPom"`" ]; then
				modules="${modules} ${moduleName}"
			fi
		done
	done

	local pomFile="$dest/pom.xml"
	create_pom "$pomFile" \
			"$(getGAV "$POM_ID.$name" "pom")" \
			"$(getGAV "$POM_ID.root" "" "$POM_ID" "$POM_VERSION" "..")" \
			"$modules"
}

aggregate_releng() {
	if [ "$CREATE_UPSITE_FROMSOURCE" == "1" ]; then
		create_compositesite_fromsource
	else
		create_compositesite_fromlink
	fi
	create_target
	
	# create releng/pom.xml
	local dest="$BUILD_DIR/releng"
    local pomFile="$dest/pom.xml"

	local profile="<id>release</id><modules><module>$POM_ID.update</module></modules>" 
    create_pom "$pomFile" \
            "$(getGAV "$POM_ID.releng" "pom")" \
            "$(getGAV "$POM_ID.root" "" "$POM_ID" "$POM_VERSION" "..")" \
            "$POM_ID.target" \
			"" \
			"$profile"
}

create_target() {
    # create releng/*.target
    # XXX
    local targetDir="$BUILD_DIR/releng/$POM_ID.target"

    mkdir -p "$targetDir"
    cp "$TARGET_FILE"  "$targetDir/$POM_ID.target.target"

    create_pom "$targetDir/pom.xml" \
            "$(getGAV "$POM_ID.target" "eclipse-target-definition")" \
            "$(getGAV "$POM_ID.releng" "" "$POM_ID" "$POM_VERSION" "..")" \
            ""
}

create_compositesite_fromsource() {
	local dest="$BUILD_DIR/releng/$POM_ID.update"
    local compCategoryFile="$dest/category.xml"

    mkdir -p "$dest"

    # Composite category file header
    cat > "$compCategoryFile" << EOF
<site>
  <description name="GeCoS Composite Release Update Site" url="$COMPOSITE_UPSITE_URL">
     GeCoS Composite update site
  </description>

EOF

    # merge features and category-def from all SUBMODULES updatesites
    local catFile=""
    for sub in $SUBMODULES; do
        [ "`basename $sub`" == "gecos-buildtools" ] && continue
        [ ! -d "$sub/releng/" ] && continue
        catFile=`find "$sub/releng" -maxdepth 2 -mindepth 2 -path "*/*.update/category.xml" -type f | head -1`
        [ ! -f "$catFile" ] && continue

        echo "<!-- copied from '$catFile' -->" >> "$compCategoryFile"
        xmlstarlet sel -t -c "//site/feature" "$catFile" >> "$compCategoryFile"
        xmlstarlet sel -t -c "//site/category-def" "$catFile" >> "$compCategoryFile"
        printf "\n\n" >> "$compCategoryFile"
    done

    # Composite category file tail
    echo "</site>" >> "$compCategoryFile"
    
    
    # create releng/*.update/pom.xml
    local pomFile="$dest/pom.xml"
    create_pom "$pomFile" \
            "$(getGAV "$POM_ID.update" "eclipse-repository")" \
            "$(getGAV "$POM_ID.releng" "" "$POM_ID" "$POM_VERSION" "..")" \
            ""


}

create_compositesite_fromlink() {
	local dest="$BUILD_DIR/releng/$POM_ID.update"
	
    mkdir -p "$dest"
	cp -r "$ROOT_DIR/releng/$POM_ID.update"/* "$dest"
}


# $1: pom file
# $2: Id
# $3: parent
# $4: modules
# $5: properties
# $6: profiles
create_pom() {
	local pomFile="$1"

	cat	> "$pomFile" << EOF
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
$2

  <parent>
$3
  </parent>

EOF

	# add modules
	if [ ! -z "$4" ]; then
		echo "  <modules>" >> "$pomFile"
		for mod in $4; do
			echo "    <module>$mod</module>" >> "$pomFile"
		done
		printf "  </modules>\n\n" >> "$pomFile"
	fi

	# add properties
	if [ ! -z "$5" ]; then
		echo "  <properties>" >> "$pomFile"
		for prop in $5; do
			echo "    $prop" >> "$pomFile"
		done
		printf "  </properties>\n\n" >> "$pomFile" 
	fi

	# add profiles
	if [ ! -z "$6" ]; then
		echo "  <profiles>" >> "$pomFile"
		for prof in $6; do
			echo "    <profile>" >> "$pomFile"
			echo "      $prof" >> "$pomFile"
			echo "    </profile>" >> "$pomFile"
		done
		printf "  </profiles>\n\n" >> "$pomFile"
	fi

	echo "</project>" >> "$1"
}

# $1: artifactId
# $2: packaging type
# $3: groupId
# $4: version
# $5: path
getGAV() {
	[ ! -z "$3" ] && echo "    <groupId>$3</groupId>"
	[ ! -z "$1" ] && echo "    <artifactId>$1</artifactId>"
	[ ! -z "$2" ] && echo "    <packaging>$2</packaging>"
	[ ! -z "$4" ] && echo "    <version>$4</version>"
	[ ! -z "$5" ] && echo "    <relativePath>$5</relativePath>"
}

##########################################
# Logging

# $1: message
info() {
    echo "[INFO] $1"
}

WARN=0
warn() {
    echo "$(tput setaf 3)[WARNING] $1$(tput sgr0)"
    ((WARN++))
}

ERRORS=0
error() {
    echo "$(tput setaf 1)[ERROR] $1$(tput sgr0)"
    ((ERRORS++))
}

##########################################
main "$@"

