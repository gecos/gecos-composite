#! /bin/bash


# update all gecos projects to the latest released (tag) version 
git submodule foreach 'if [ "$name" != "gecos-buildtools" ]; then
	git fetch --tags;
#	latest=`git describe --abbrev=0`;
	latest=`git tag -l v[0-9]*.[0-9]*.[0-9]* | sort -r | head -1`;
	echo "Update submodule $name to version $latest ...";
	git checkout $latest;
fi || : '