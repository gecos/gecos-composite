# Build all GeCoS projects from sources

The bash script `build.sh` can be used to create a composite GeCoS
project structure with all the bundles, features and tests
from all the submodules (GeCoS projects) as well a composite update site.

- It creates the file structure in the directory `build`,
which is first cleaned if it already exists.
- It links all the bundles, features and tests
from all the submodules to corresponding locations in `build`.
- It creates all the necessary `pom.xml` files
- It invokes `mvn` from `build`.
The command line arguments, if specified, are used as mvn options.
Otherwise, the script exits after constructing the composite project structure. 

**WARNING:** this script is only tested on ubuntu1604.
