import java.io.File
import java.text.SimpleDateFormat
import java.util.regex.Pattern
import java.util.zip.ZipOutputStream
import java.util.zip.ZipEntry
import java.nio.channels.FileChannel

class CompositeSiteGenerator {
	
	def outDir
	def repoDir
	def rootDir
	
	CompositeSiteGenerator(String outputDir, String submodulesDir) {
		rootDir = new File(submodulesDir);
		outDir = new File(outputDir);
		repoDir = new File(outDir, 'repository')
		repoDir.mkdirs();
	}
	
	
	void generateP2Index(String filename, Map model) {
		def template = new groovy.text.StreamingTemplateEngine().createTemplate '''\
version=1
metadata.repository.factory.order=$compositeContentFile,\\!
artifact.repository.factory.order=$compositeArtifactFile,\\!'''
		
		def content = template.make(model)
		new File(repoDir, filename).withWriter('UTF-8') {
			writer -> writer.write content
		}
	}
	
	void generateCompositeFile(String filename, Map model) {
		def config = new groovy.text.markup.TemplateConfiguration()
		config.setAutoNewLine(true);
		config.setAutoIndent(true);
		def engine = new groovy.text.markup.MarkupTemplateEngine(config)
		def template = engine.createTemplate '''
		xmlDeclaration()
		repository (name : repname, type : reptype, version : repversion) {
			properties (size : '1') {
				property (name : 'p2.timestamp', value : timestamp)
			}
			newLine() 
			children (size : children.size) {
				children.each {
					child (location : it)
					newLine()
				}
			}
		}
		'''
		
		def file = new File(repoDir, filename)
		template.make(model).writeTo(new PrintWriter(file))
	}
	
	String getSiteUrl(File submoduleDir) {
		def name =  submoduleDir.name.replaceFirst(Pattern.compile("gecos-"), '')
		
		// find current submodule version
		def proc = "git -C $submoduleDir describe --abbrev=0".execute()
		def b = new StringBuffer()
		proc.consumeProcessErrorStream(b)
		def version = proc.text.readLines().get(0).stripIndent()
		if(b.size() != 0)
			throw new RuntimeException(b)
		if(version.find(Pattern.compile("v[0-9]+.[0-9]+.[0-9]+")) == null)
			throw new RuntimeException("Not a release version: module '$name' version '$version' !")
			
		def url = "http://gecos.gforge.inria.fr/updatesite/gecos/$name/release/$version"
		
		//TODO check if url is valid
		//!!! old gecos-tools-* updatesites are located elsewhere !!! move them (keep a copy)
			
		return url
	}
	
	
	void generate() {
		def COMPOS_INDEX_FILE = 'p2.index'
		def COMPOS_CONT_FILE  = 'compositeContent.xml'
		def COMPOS_ARTI_FILE  = 'compositeArtifacts.xml'
		
		def updatesiteName = 'GeCoS composite'
		def timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
		
		def childrenURLs = []
		this.rootDir.eachDirMatch(~/gecos-(?!buildtools).*/) {
			childrenURLs << getSiteUrl(it)
		}
		if(childrenURLs.size() == 0)
			throw new RuntimeException("Couldn't find gecos submodules in rootDir '$rootDir' !")
		
		generateP2Index(COMPOS_INDEX_FILE, [
			compositeContentFile  : COMPOS_CONT_FILE,
			compositeArtifactFile : COMPOS_ARTI_FILE
		])
		
		generateCompositeFile(COMPOS_CONT_FILE, [
			repname   : updatesiteName,
			reptype   : 'org.eclipse.equinox.internal.p2.metadata.repository.CompositeMetadataRepository',
			repversion: '1',
			timestamp : timestamp,
			children  : childrenURLs
		])
		
		generateCompositeFile(COMPOS_ARTI_FILE, [
			repname   : updatesiteName,
			reptype   : 'org.eclipse.equinox.internal.p2.artifact.repository.CompositeArtifactRepository',
			repversion: '1',
			timestamp : timestamp,
			children  : childrenURLs
		])
		
	}
	
	void zip(String projectName) {
		def zipFileName = new File(outDir, "${projectName}.zip")
		ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(zipFileName))
		
		repoDir.eachFile() { file ->
			zipFile.putNextEntry(new ZipEntry(file.getName()))
			def buf = new byte[1024]
			file.withInputStream { i ->
				int len
				while ((len = i.read(buf)) > 0) {
					zipFile.write(buf, 0, len);
				}
			}
			zipFile.closeEntry()
		}
		
		zipFile.close()
	}
	
}

/**********************/

def outputDir = properties['GenerateCompositeSite.outputDir']
def rootDir = properties['GenerateCompositeSite.rootDir']
def projectName = properties['GenerateCompositeSite.projectName']

def generator = new CompositeSiteGenerator(outputDir, rootDir)
generator.generate()
generator.zip(projectName)



